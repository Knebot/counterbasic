﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterClass
{
    public class Counter
    {
        private static int value;
        public static int  Incrementation()
        {
            
             return ++value; 
        }
        public static int Decrementation()
        {
            
            return --value;
        }
        public static int  Reset()
        {
            value = 0;
            return value;
        }
            public static int Values
        {
            get { return value; }

        }
    }
}
