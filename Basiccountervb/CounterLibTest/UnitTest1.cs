﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterClass;

namespace CounterLibTest
{
    [TestClass]
    public class UnitTest1
    {

        public class TestUnitaire
        {
            [TestMethod]
            public void TestIncrementation()
            {

                Assert.AreEqual(1, Counter.Incrementation());
                Assert.AreEqual(4, Counter.Incrementation());
            }
            [TestMethod]
            public void TestDecrementation()
            {
                Assert.AreEqual(-1, Counter.Decrementation());
                Assert.AreEqual(8, Counter.Decrementation());

            }
            [TestMethod]
            public void TestReset()
            {
                Assert.AreEqual(0, Counter.Reset());
                Assert.AreEqual(10, Counter.Reset());

            }
                }
    }
}
}
